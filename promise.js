function positiveNumber(num) {
    return new Promise((resolve, reject) => {
        if (num % 2 === 0) {
            return resolve(`ok ${num}`)
        }

        return reject(new Error(`failed ${num}`))
    })
}

async function wait() {
    try {
        let output = await positiveNumber(1)
        console.log(output)
    } catch (e) {
        console.log(e.message)
    } finally {
        console.log('done')
    }
    
    try {
        let output = await positiveNumber(4)
        console.log(output)
    } catch (e) {
        console.log(e.message)
    } finally {
        console.log('done')
    }
}

wait()

// positiveNumber(1)
//     .then(result => {
//         console.log(result)
//     })
//     .catch(err => console.log(err.message))
//     .finally(() => {
//         console.log("done")
//         positiveNumber(4)
//             .then(result => console.log(result))
//             .catch(err => console.log(err.message))
//             .finally(() => console.log("done"))
//     })
