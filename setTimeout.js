console.log(new Date())

let counter = 0
function timeout() {
    if (counter === 3) {
        return
    }
    setTimeout(() => {
        console.log("hello world")
        console.log(new Date())
        timeout()
    }, 1000)

    counter++
}

timeout()
