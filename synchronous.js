const fs = require("fs")

// let output = fs.readFileSync("contoh.txt", {})
// console.log(output.toString())

async function wait() {

    try {
        let output = await new Promise((resolve, reject) => {
            setTimeout(() => {
                reject(new Error("error sample"))
            }, 1000)
        })
    
        console.log(output)
    } catch (e) {
        console.log(e.message)
    }

    console.log("done")
}

wait()

